//
//  Track.swift
//  TrackListApp
//
//  Created by Maksim on 25.02.2022.
//

struct Track {
    let artist: String
    let song: String
    
    var track: String {
        "\(artist) - \(song)"
    }
}

extension Track {
    static func getTrackList() -> [Track] {
        return [
            Track(artist: "Rammstein", song: "Mutter"),
            Track(artist: "Slaughter To Prevail", song: "Bratva"),
            Track(artist: "Inova", song: "Insomnia"),
            Track(artist: "Slipknot", song: "Killpop"),
            Track(artist: "Korn", song: "Cold"),
            Track(artist: "Disturbed", song: "No More"),
            Track(artist: "Trivium", song: "Sickness Unto You"),
            Track(artist: "Silent Planet", song: "Till We Have Faces"),
            Track(artist: "Smash Into Pieces", song: "Broken Parts"),
            Track(artist: "Battle Beast", song: "No More Hollywood Endings")
        ]
    }
}
