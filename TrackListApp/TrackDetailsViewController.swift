//
//  TrackDetailsViewController.swift
//  TrackListApp
//
//  Created by Maksim on 25.02.2022.
//

import UIKit

class TrackDetailsViewController: UIViewController {

    @IBOutlet var artCoverImageView: UIImageView!
    @IBOutlet var trackLabel: UILabel!
    
    var track: Track! // force unwrap для упрощения
    override func viewDidLoad() {
        super.viewDidLoad()
        
        artCoverImageView.image = UIImage(named: track.track)
        trackLabel.text = track.track
        
        
    }
    



}
